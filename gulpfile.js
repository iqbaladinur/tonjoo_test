'use strict';
const gulp      = require('gulp');
const sass      = require('gulp-sass'); 
const minify    = require('gulp-csso');
const concat    = require('gulp-concat');
const sourcemaps= require('gulp-sourcemaps');
const sassglob  = require('gulp-sass-glob');
sass.compiler   = require('node-sass');
gulp.task('sass', function () {
    return gulp.src('./src/sass/main.scss')
        .pipe(sassglob())
        .pipe(sass().on('error', sass.logError))
        .pipe(minify())
        .pipe(gulp.dest('./dist/css'));
});
gulp.task('js', function () {
    return gulp.src('./src/js/*.js')
        .pipe(sourcemaps.init())
        .pipe(concat('app.min.js'))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('./dist/js'))
});
gulp.task('asset', function () {
    return gulp.src('./static/*.*')
        .pipe(gulp.dest('./dist/static'));
})
gulp.task('default', function () {
    gulp.watch(
        ['./src/sass/**/*.scss', './src/js/*.js', './static/*.*'], 
        ['sass', 'js', 'asset']
    );
});